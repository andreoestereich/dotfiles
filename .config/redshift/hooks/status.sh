#!/bin/sh

# Change the file to contain RS status

if [ "$1" = period-changed ]; then
	case $3 in
		night)
			echo "🌚" > $HOME/.config/redshift/stat
			;;
		transition)
			echo "🌙" > $HOME/.config/redshift/stat
			;;
		daytime)
			echo "🌞" > $HOME/.config/redshift/stat
			;;
	esac
    pkill -RTMIN+3 lemonblocks
fi
