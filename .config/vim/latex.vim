" latex snipets


" Operate on LaTeX quotes
vmap <buffer> iq <ESC>?``<CR>llv/''<CR>h
omap <buffer> iq :normal viq<CR>
vmap <buffer> aq <ESC>?``<CR>v/''<CR>l
omap <buffer> aq :normal vaq<CR>

" set spellcheck
setlocal spell
set spelllang=en_us
set omnifunc=syntaxcomplete#Complete

" autoopeen pdf
map <leader>i :!zathura %:r.pdf & <Enter><Enter>

set mps+=$:$

" open tabs as spaces
" Creating environments
inoremap çeq \begin{equation}<Enter>\label{<++>}<Enter>\end{equation}<Enter><++><Esc>2kO
inoremap çee \begin{equation*}<Enter>\end{equation*}<Enter><++><Esc>kO
inoremap çit \begin{itemize}<Enter>\item <Enter>\end{itemize}<Enter><++><Esc>2kA
inoremap çfg \begin{figure}[h]<Enter>\centering<Enter>\includegraphics[width=0.5\textwidth]{}<Enter>\caption{<++>}<Enter>\label{<++>}<Enter>\end{figure}<Enter><Enter><++><Esc>5kf{a
inoremap çsf \begin{subfigure}[t]{<++>\textwidth}<Enter>\includegraphics[width=\textwidth]{<++>}<Enter>\caption{<++>}<Enter>\end{subfigure}<Enter>\hfill<Enter><++><Esc>5kf<cf>
inoremap çfr \begin{frame}<Enter>\frametitle{}<Enter><Enter><++><Enter>\end{frame}<Enter><++><Esc>4kf{a
inoremap çta \begin{center}<Enter>\begin{tabular}{<++>}<Enter><++><Enter>\end{tabular}<Enter>\end{center}<Enter><++>
inoremap çex \vspace{2em}\textbf{Exemplo:}<Enter><Enter>\vspace{1em}\textbf{Solução:}<++><Enter><Enter>\vspace{2em}<++><Esc>4kA

" creating tags
inoremap çrf \cref{}<++><Esc>F{a
inoremap çct \cite{}<++><Esc>F{a
inoremap çvv \vec{}<++><Esc>F{a
inoremap çni \noindent<space>
inoremap çch \chapter{}<++><Esc>F{a
inoremap çse \section{}<++><Esc>F{a
inoremap çss \subsection{}<++><Esc>F{a
inoremap çla \label{}<++><Esc>F{a
inoremap çtb \textbf{}<++><Esc>F{a
inoremap çem \emph{}<++><Esc>F{a
inoremap çca \caption{}<++><Esc>F{a
inoremap çqu \quad
inoremap çqq \qquad
inoremap çal [allowframebreaks]
inoremap çft \framesubtitle{}<++><Esc>F{a
inoremap çfc \framesubtitle{}<++><Esc>F{a
inoremap çss \SI{}{<++>}<++><Esc>2F{a
inoremap çpp \problem{}<Enter>\vspace{2em}<Enter><++><Esc>2k0f{a
inoremap çan \answers{}{<++>}{<++>}{<++>}{<++>}<++><Esc>Fsla



inoremap çkk  \langle k \rangle
inoremap çmm  \mu_{ij}

" fast math
inoremap çfa \forall<space>
inoremap __ _{}<++><Esc>F{a
inoremap ^^ ^{}<++><Esc>F{a
inoremap /= \neq
inoremap <= \leq
inoremap >= \geq
inoremap ç/ \frac{}{<++>}<++><Esc>2F{a
inoremap ~+ \sum
inoremap ~~ \sim
inoremap ç2 \sqrt{}<++><Esc>F{a
inoremap .. \cdot<Space>

" enclosing
inoremap [[ \left[ \right]<++><Esc>F[a
inoremap {{ \left\{ \right\}<++><Esc>F{a
inoremap (( \left( \right)<++><Esc>F(a
inoremap "" ``''<++><Esc>F`a

" Creating greek letters
inoremap  #G \Gamma
inoremap  #D \Delta
inoremap  #L \Lambda
inoremap  #F \Phi
inoremap  #P \Pi
inoremap  #W \Psi
inoremap  #S \Sigma
inoremap  #T \Theta
inoremap  #U \Upsilon
inoremap  #X \Xi
inoremap  #O \Omega

inoremap  #a \alpha
inoremap  #b \beta
inoremap  #g \gamma
inoremap  #d \delta
inoremap  #ee \epsilon
inoremap  #z \zeta
inoremap  #et \eta
inoremap  #t \theta
inoremap  #i \iota
inoremap  #k \kappa
inoremap  #l \lambda
inoremap  #m \mu
inoremap  #n \nu
inoremap  #x \xi
inoremap  #p \pi
inoremap  #r \rho
inoremap  #s \sigma
inoremap  #t \theta
inoremap  #u \upsilon
inoremap  #f \phi
inoremap  #c \chi
inoremap  #w \psi
inoremap  #o \omega
inoremap  #ve \varepsilon
inoremap  #vk \varkappa
inoremap  #vf \varphi
inoremap  #vp \varpi
inoremap  #vr \varrho
inoremap  #vs \varsigma
inoremap  #vt \tau

" math symbols
inoremap  ~< \langle
inoremap  ~> \rangle
inoremap  ~8 \infty
inoremap  'l \ell
inoremap  ° {}^\circ
inoremap  çxx \times
inoremap  çtt \text{}<++><Esc>F{a
inoremap  ~v \vec{}<++><Esc>F{a

" acentos
inoremap  ~a ã
inoremap  ~A Ã
inoremap  ~o õ
inoremap  ~O Õ
inoremap  'a á
inoremap  'A Á
inoremap  'e é
inoremap  'E É
inoremap  'i í
inoremap  'I Í
inoremap  'o ó
inoremap  'O Ó
inoremap  'u ú
inoremap  'U Ú
inoremap  ^a â
inoremap  ^e ê
inoremap  ^o ô
inoremap  ^A Â
inoremap  ^E Ê
inoremap  ^O Ô
