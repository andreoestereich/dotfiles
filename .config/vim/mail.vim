" This is the configs for groff files
setlocal spell
set tw=60

set spelllang=pt_br

" Enable Goyo by default for mutt writting
" let g:goyo_width=80
" :Goyo | set bg=light
" map ZZ :Goyo\|x!<CR>
" map ZQ :Goyo\|q!<CR>

" special characters
inoremap ~a ã
inoremap ^a â
inoremap 'a á
inoremap ^e ê
inoremap 'e é
inoremap 'E É
inoremap 'i í
inoremap 'I Í
inoremap ~o õ
inoremap ^o ô
inoremap ^O Ô
inoremap 'o ó
inoremap 'O Ó
inoremap 'u ú
inoremap 'U Ú
inoremap çç Abraço,<Enter>André.
inoremap ç= ≠
inoremap ç8 ∞

" greek letters
inoremap .a α
inoremap .b β
inoremap .g γ
inoremap .d δ
inoremap .e ε
inoremap .z ζ
inoremap .e η
inoremap .t θ
inoremap .i ι
inoremap .k κ
inoremap .l λ
inoremap .m μ
inoremap .n ν
inoremap .x ξ
inoremap .o ο
inoremap .p π
inoremap .r ρ
inoremap .s σ
inoremap .t τ
inoremap .u υ
inoremap .p φ
inoremap .c χ
inoremap .p ψ
inoremap .z ω
inoremap .A Α
inoremap .B Β
inoremap .G Γ
inoremap .D Δ
inoremap .E Ε
inoremap .Z Ζ
inoremap .E Η
inoremap .T Θ
inoremap .I Ι
inoremap .K Κ
inoremap .L Λ
inoremap .M Μ
inoremap .N Ν
inoremap .X Ξ
inoremap .O Ο
inoremap .P Π
inoremap .R Ρ
inoremap .S Σ
inoremap .T Τ
inoremap .U Υ
inoremap .P Φ
inoremap .C Χ
inoremap .P Ψ
inoremap .Z Ω
